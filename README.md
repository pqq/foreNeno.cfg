# foreNeno.cfg
publicly available [configuration files](https://github.com/klevstul/foreNeno.cfg/tree/master/cfg) for the foreNeno platform.

### foreNeno
for more information about foreNeno visit [foreNeno.com](http://foreNeno.com).
